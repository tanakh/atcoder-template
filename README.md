# atcoder-template

## Usage

Install [cargo-generate](https://github.com/ashleygwilliams/cargo-generate).

```bash
cargo install cargo-generate
```

Then,

```bash
cargo generate --git https://gitlab.com/tanakh/atcoder-template.git
```
